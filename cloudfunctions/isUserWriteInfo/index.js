// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  var _openId = event.openId;
  var upperId = event.upperId;
  var databaseName = event.databaseName
  console.log('[isUserWriteInfo]函数获取到的数据:');
  console.log('_openId: ', _openId)
  console.log('upperId: ', upperId)
  console.log('databaseName: ', databaseName)
  return await db.collection(databaseName).where({
    _openid: _openId,
    upperId: upperId
  }).count()
}