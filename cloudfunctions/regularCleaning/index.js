// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {
  // 三天前
  var oldtime = db.serverDate({
    offset: -60*60*1000*24*3
  })
  const _ = db.command;
  // 定时清理各个集合三天前的数据数据
  // 清理notices集合
  await db.collection('notices').where({
    serverTime: _.lt(oldtime)
  }).remove()
  // 清理collects集合
  await db.collection('collects').where({
    serverTime: _.lt(oldtime)
  }).remove()
  // 清理pollvote集合
  await db.collection('pollvote').where({
    serverTime: _.lt(oldtime)
  }).remove()
  // 清理tickets集合
  await db.collection('tickets').where({
    serverTime: _.lt(oldtime)
  }).remove()
  // 清理ticketResult
  await db.collection('ticketResult').where({
    serverTime: _.lt(oldtime)
  }).remove()
  // 清理collectResult
  await db.collection('collectResult').where({
    serverTime: _.lt(oldtime)
  }).remove()
}