//app.js
App({
  onLaunch: function (opt) {
    console.log(opt)
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        // 使用正式环境
        env: 'mr-joker-8d37e6',
        traceUser: true,
      })
    }

    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (!res.authSetting['scope.userInfo']) {
          // 未授权，跳转到授权页面
          wx.redirectTo({
            url: 'index',
          })
        }
      }
    })

    // // 调用云函数
    // wx.cloud.callFunction({
    //   name: 'login',
    //   data: {},
    //   success: res => {
    //     console.log('[云函数] [login] user openid: ', res.result.openid)
    //     this.globalData.openid = res.result.openid
    //     // wx.navigateTo({
    //     //   url: '../userConsole/userConsole',
    //     // })
    //   },
    //   fail: err => {
    //     console.error('[云函数] [login] 调用失败', err)
    //     wx.navigateTo({
    //       url: '../deployFunctions/deployFunctions',
    //     })
    //   }
    // })

    this.globalData = {
      openid: "",
      logged: false,
      query: {},
      lists: [{
        tag: "收集",
        id: "collect",
        icon: "form",
        color: "green",
        name: "收集盒",
        status: false
      }, {
        tag: "投票",
        id: "vote",
        icon: "ticket",
        color: "yellow",
        name: "投票箱",

      }, {
        tag: "通告",
        id: "noticeWall",
        icon: "focus",
        color: "brown",
        name: "通告墙",
        status: true
      }, {
        id: "album",
        icon: "pic",
        color: "blue",
        name: "生活角",
        status: true,
      }, {
        id: "folder",
        icon: "file",
        color: "cyan",
        name: "文件夹",
        status: false
      }, {
        id: "sofa",
        icon: "video",
        color: "olive",
        name: "沙发",
        status: false
      },{
        id: "about",
        icon: "info",
        color: "olive",
        name: "关于",
        status: true
      }]}
  },
  onShow: function (e) {
    this.shareTicket = e.shareTicket;
    console.log(e)
    this.globalData.shareTicket = e.shareTicket;
    // 从群聊入口进来的处理
    // 获取当前要进入的页面
    var pagepath = e.path;
    // 获取分享页面所传递的数据组
    var query = e.query;
    this.globalData.query = query;
    // if (pagepath === 'pages/noticeWall/noticeWall') {
    //   // 进入通告墙界面
    //   // 获取
    // } else if (pagepath === 'pages/album/album') {
    //   // 进入生活照界面
    // } else if (pagepath === 'pages/files/files') {
    //   // TODO 文件夹功能
    // } else if (pagepath === 'pages/sofa/sofa') {
    //   // TODO 沙发功能
    // } else {
    //   // do noting
    // }

  },

  /**
   * 获取群聊ID
   */
  getShareTiket: function (cb) {
    let self = this;
    /**
     * 如果是分享到群聊中
     */
    if (self.globalData.shareTicket) {
      wx.getShareInfo({
        shareTicket: self.globalData.shareTicket,
        success: function (res) {
          console.log('[app.js] [getShartTicket] res: ', JSON.stringify(res));
          // 获取encryptedData,iv
          let js_encryptedData = res.encryptedData;
          let js_iv = res.iv;
          wx.checkSession({
            success: function(res) {
              console.log('[checkSession] success message: ', res)
              var js_code = wx.getStorageSync('js_code');
              var sessionKey = wx.getStorageSync('sessionKey');
              console.log('has session', js_code)
              wx.cloud.callFunction({
                name: 'decrypt',
                data: {
                  js_code: js_code,
                  appid: 'wx6f0580c7c129c2f9',
                  encryptedData: js_encryptedData,
                  iv: js_iv,
                  sessionKey: sessionKey
                },
                success: function (res) {
                  console.log('[app.js] [140]获取到的opengid: ', res.result[0].openGId);
                  console.log('[app.js] [141]调用decrypt云函数的返回结果：', JSON.stringify(res));
                  self.globalData.openGid = res.result.openGId;
                  typeof cb == "fuction" && cb(that.globalData)
                },
                fail: function (err) {
                  console.log('[app.js] [getShartTiket] err message: ', err)
                }
              })
            },
            fail: function(err) {
              console.log('[checkSession] fail message: ', err)
              // 重新登录
              wx.login({
                success: function (res) {
                  // get code
                  console.log('[app.js] [login] code: ', res.code);
                  wx.setStorage({
                    key: 'js_code',
                    data: res.code,
                  })
                  // 调用云函数，破解opengId
                  wx.cloud.callFunction({
                    name: 'decrypt',
                    data: {
                      js_code: res.code,
                      appid: 'wx6f0580c7c129c2f9',
                      encryptedData: js_encryptedData,
                      iv: js_iv,
                      sessionKey: ''
                    },
                    success: function (res) {
                      console.log('[app.js] [140]获取到的opengid: ', res.result[0].openGId);
                      console.log('[app.js] [141]调用decrypt云函数的返回结果：', res);
                      self.globalData.openGid = res.result.openGId;
                      wx.setStorage({
                        key: 'sessionKey',
                        data: res.result[1].sessionKey,
                      })
                      typeof cb == "fuction" && cb(that.globalData)
                    },
                    fail: function (err) {
                      console.log('[app.js] [getShartTiket] err message: ', err)
                    }
                  })
                }
              })
            }
          })
          // wx.login({
          //   success: function (res) {
          //     // get code
          //     console.log('[app.js] [login] code: ', res.code);
          //     // 调用云函数，破解opengId
          //     wx.cloud.callFunction({
          //       name: 'decrypt',
          //       data: {
          //         js_code: res.code,
          //         appid: 'wx6f0580c7c129c2f9',
          //         encryptedData: js_encryptedData,
          //         iv: js_iv
          //       },
          //       success: function (res) {
          //         console.log('[app.js] [140]获取到的opengid: ', res.result.openGId);
          //         console.log('[app.js] [141]调用decrypt云函数的返回结果：',JSON.stringify(res));
          //         self.globalData.openGid = res.result.openGId;
          //         typeof cb == "fuction" && cb(that.globalData)
          //       },
          //       fail: function (err) {
          //         console.log('[app.js] [getShartTiket] err message: ', err)
          //       }
          //     })
          //   }
          // })
        }
      })
    }
  }
})
