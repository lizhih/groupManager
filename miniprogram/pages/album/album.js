// pages/album/album.js
const app = getApp();
const dateFormatUtil = require("../../utils/dateFormatUtil.js"); // 导入日期格式化工具

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShowUserInfo: false, // 用户操作弹出层显示控制
    isUserPopup: false, // 用户操作弹出效果控制
    lists: app.globalData.lists,
    avatarUrl: '', // 用户头像
    imgList: [], // 上传图片列表
    isUploadPopup: false,
    isShowUploadPopup: false,
    dataList: [], // 获取该页面数据
  },

  /**
   * 图片预览
   */
  changePreview: function(e) {
    var self = this;
    var _url = e.currentTarget.id;
    var _lists = this.data.dataList;
    var _urls = [];
    // 循环查询当前选中图片的所有图片组
    _lists.forEach((item) => {
      item.urls.forEach((url) => {
        if (url === _url) {
          _urls = item.urls
        }
      })
    })
    wx.previewImage({
      current: _url,
      urls: _urls,
    })
  },

  // 如果用户已登录则显示用户操作列表
  onGetUserInfo: function (e) {
    if (app.globalData.openid === undefined) {
      this.onGetOpenid();
    }
    if (!this.logged && e.detail.userInfo) {
      this.setData({
        logged: true,
        avatarUrl: e.detail.userInfo.avatarUrl,
        userInfo: e.detail.userInfo
      })
    }
    // 修改用户弹出层
    this.toggleUserPopup();
  },
  // 修改用户弹出层
  toggleUserPopup: function () {
    var that = this;
    this.setData({
      isUserPopup: !this.data.isUserPopup,
    })
    if (this.data.isUserPopup === true) {
      this.setData({
        isShowUserInfo: true
      })
    } else {
      setTimeout(function () {
        that.setData({
          isShowUserInfo: false
        })
      }, 500);
    }
  },
  // 功能跳转
  goToOtherPage: function (e) {
    var _url = e.currentTarget.id;
    var that = this;
    wx.redirectTo({
      url: "../" + _url + "/" + _url,
      fail: (res) => {
        wx.showModal({
          title: "非常抱歉",
          content: "程序员被祭天了,该功能还未上线",
        })
      },
      complete: (res) => {
        that.toggleUserPopup();
      }
    })
  },

  toggleAddPopup: function () {
    var that = this;
    this.setData({
      isUploadPopup: !this.data.isUploadPopup,
    })
    if (this.data.isUploadPopup === true) {
      this.setData({
        isShowUploadPopup: true
      })
    } else {
      setTimeout(function () {
        that.setData({
          isShowUploadPopup: false
        })

      }, 500)
    }
  },

  /**
   * 从本地选择图片
   */
  ChooseImage() {
    var self = this;
    wx.chooseImage({
      count: 6,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        if (self.data.imgList.length != 0) {
          self.setData({
            imgList: self.data.imgList.concat(res.tempFilePaths)
          })
        } else {
          self.setData({
            imgList: res.tempFilePaths
          })
        }
      },
    })
  },

  /**
   * 删除所选择的图片
   */
  DelImg(e) {
    this.data.imgList.splice(e.currentTarget.dataset.index, 1);
    this.setData({
      imgList: this.data.imgList
    })
  },

  /**
   * 预览要添加的图片
   */
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    })
  },

  deleteImageValue: function () {
    this.setData({
      imgList: []
    })
    // 隐藏
    this.toggleAddPopup();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.showShareMenu({
      withShareTicket: true
    })
    // 获取用户头像信息
    this.setData({
      avatarUrl: app.globalData.avatarUrl
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var self = this;
    // 查询指定用户的数据
    self.queryAlbumByOpenId(app.globalData.openid);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var self = this;
    var fileIds = [];
    // 将照片组保存到存储库中
    var _imgList = this.data.imgList;
    // 上传图片
    for (var i = 0; i < _imgList.length; i++) {
      var filePath = _imgList[i];
      var fileSuffix = _imgList[i].match(/\.[^.]+?$/)[0];
      // 路径 + 文件名 + 后缀名
      var cloudPath = 'images/' + Math.random() * 100000000 + fileSuffix;
      wx.cloud.uploadFile({
        cloudPath: cloudPath,
        filePath: filePath
      }).then(res => {
        fileIds.push(res.fileID)
      })
    }
    // 设置延迟执行保存操作，解决异步问题
    setTimeout(function () {
      // 将图片路径保存到数据集合中
      self.addAlbum(fileIds);
    }, 5000)
  },

  //=========================数据库语句===============================


  /*------------album集合的增删改查 start---------------------------*/

  /**
   * 添加一条图片组记录
   * 格式为：{
   *  _id: 自动生成,
   *  _openid: 用户唯一id,
   *  urls: 图片fileId组，
   *  createTime: 创建时间,
   *  userInfo: 用户信息
   * }
   * 
   * 
   */
  addAlbum: function (urls) {
    var self = this;
    var createTime = dateFormatUtil.formatTime(new Date());
    var userInfo = app.globalData.userInfo;
    const db = wx.cloud.database();
    db.collection('album').add({
      data: {
        urls: urls,
        createTime: createTime,
        userInfo: userInfo
      }
    })
  },

  /**
   * 根据_openid查询album集合数据
   */
  queryAlbumByOpenId: function (openId) {
    const self = this;
    const db = wx.cloud.database();
    db.collection('album').where({
      _openid: openId
    }).orderBy('createTime', 'desc').get({
      success(res) {
        var array = res.data;
        array.forEach(item => {
          // 根据页面数据展现需求对日期做相应的格式化处理
          var dateTime = item.createTime.split(' ');
          item['formatDate'] = dateTime[0].substr(5, 10);
          item['formatTime'] = dateTime[1].substr(0, 5);
        })
        self.setData({
          dataList: array
        })
      }
    })
  }

  /*------------album集合的增删改查 start---------------------------*/
})