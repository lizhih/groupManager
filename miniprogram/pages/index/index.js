//index.js
const app = getApp()

Page({
  data: {
    avatarUrl: './user-unlogin.png',
    userInfo: {},
    logged: false,
    takeSession: false,
    requestResult: ''
  },

  onLoad: function() {
    if (!wx.cloud) {
      wx.redirectTo({
        url: '../chooseLib/chooseLib',
      })
      return
    }

    // 获取用户信息
    wx.getSetting({
      success: (res) => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，直接跳转到功能页面
          wx.getUserInfo({
            success: (res) => {
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo
              })
              if (app.globalData.openid == "") {
                this.onGetOpenid()
              }
              app.globalData.avatarUrl = res.userInfo.avatarUrl;
              app.globalData.userInfo = res.userInfo
            }
          })
        }
      }
    })
  },

  onGetUserInfo: function(e) {
    if (app.globalData.openid == "") {
      this.onGetOpenid()
    }
    if (!this.logged && e.detail.userInfo) {
      this.setData({
        logged: true,
        avatarUrl: e.detail.userInfo.avatarUrl,
        userInfo: e.detail.userInfo
      })
      app.globalData.avatarUrl = e.detail.userInfo.avatarUrl;
      app.globalData.userInfo = e.detail.userInfo
    }
  },

  onGetOpenid: function() {
    // 调用云函数
    wx.cloud.callFunction({
      name: 'login',
      data: {},
      success: (res) => {
        console.log('[云函数] [login] user openid: ', res.result.openid)
        console.log('[云函数] [login] res: ', res);
        app.globalData.openid = res.result.openid;
        app.globalData.appid = res.result.appid;
        // issue 6
        // 进行页面跳转，因为在调用云函数的时候存在异步回调，导致在通告墙初始化的时候无法及时获取openid
        wx.redirectTo({
          url: '../noticeWall/noticeWall',
        })
      },
      fail: (err) => {
        console.error('[云函数] [login] 调用失败', err)
        wx.showToast({
          title: '云函数调用失败',
        })
      }
    })
  },

  // 上传图片
  doUpload: function () {
    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {

        wx.showLoading({
          title: '上传中',
        })

        const filePath = res.tempFilePaths[0]
        
        // 上传图片
        const cloudPath = 'my-image' + filePath.match(/\.[^.]+?$/)[0]
        wx.cloud.uploadFile({
          cloudPath,
          filePath,
          success: (res) => {
            console.log('[上传文件] 成功：', res)

            app.globalData.fileID = res.fileID
            app.globalData.cloudPath = cloudPath
            app.globalData.imagePath = filePath
            
            wx.navigateTo({
              url: '../storageConsole/storageConsole'
            })
          },
          fail: (e) => {
            console.error('[上传文件] 失败：', e)
            wx.showToast({
              icon: 'none',
              title: '上传失败',
            })
          },
          complete: () => {
            wx.hideLoading()
          }
        })

      },
      fail: (e) => {
        console.error(e)
      }
    })
  },

})
