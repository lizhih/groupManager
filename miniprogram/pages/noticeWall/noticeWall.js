// pages/noticeWall/noticeWall.js
const app = getApp();
const dateFormatUtil = require("../../utils/dateFormatUtil.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: '',
    userInfo: {},
    takeSession: false,
    requestResult: '',
    menuFixed: false,
    menuTop: 0,
    isShowCardInfo: false, // 卡片弹出层显示控制
    isPopup: false, // 卡片弹出效果控制
    isShowUserInfo: false, // 用户操作弹出层显示控制
    isUserPopup: false, // 用户操作弹出效果控制
    isAddPopup: false, // 添加按钮弹出效果控制
    isAddShowPopup: false, // 添加按钮弹出层控制
    isCreatePopup: false, // 添加框弹出效果
    isShowCreateInfo: false, // 添加框显示控制
    currentList: {}, // 当前选中的类型
    formContent: {}, // 表单内容
    isForm: true, // 显示输入
    lists: app.globalData.lists,
    noticeInfos: [], // 通告数据
    cOrTData: [], // 收集或投票数据
    currentInfo: [], // 当前选择卡片的信息
    createUserId: '', // 创建该内容的用户Id
    currentCardTitle: '', // 当前所选卡片标题
    ticketAvators: [], // 投票结果显示头像组
    ticketTotal: 0, // 投票总数
    ticketResult: {}, // 投票选项
    createSuccessId: '' // 快速添加成功后返回的_id
    //showSkeleton: false // 骨架屏显示隐藏
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.showShareMenu({
      withShareTicket: true
    })
    console.log(app.globalData)
    // 获取用户头像和信息
    this.setData({
      avatarUrl: app.globalData.avatarUrl,
      userInfo: app.globalData.userInfo
    })
    // 查询需要吸顶的元素距离顶端的距离
    var that = this;
    var query = wx.createSelectorQuery()
    query.select("#affix").boundingClientRect()
    query.exec(function(res) {
      that.setData({
        menuTop: res[0].top
      })
    })
  },

  // 获取页面移动距离，实现吸顶效果
  onPageScroll: function(scroll) {
    var that = this;
    if (that.data.menuFixed === (scroll.detail.scrollTop > that.data.menuTop)) {
      return;
    }
    // 3.当页面滚动距离scrollTop > menuTop菜单栏距离文档顶部的距离时，菜单栏固定定位
    that.setData({
      menuFixed: (scroll.detail.scrollTop > that.data.menuTop)
    })
  },

  // 如果用户已登录则显示用户操作列表
  onGetUserInfo: function(e) {
    // 修改用户弹出层
    this.toggleUserPopup();
  },
  // 修改用户弹出层
  toggleUserPopup: function() {
    var that = this
    this.setData({
      isUserPopup: !this.data.isUserPopup,
    })
    if (this.data.isUserPopup === true) {
      this.setData({
        isShowUserInfo: true
      })
    } else {
      setTimeout(function() {
        that.setData({
          isShowUserInfo: false
        })
      }, 500);
    }
  },

  // 修改添加按钮弹出层
  toggleAddPopup: function() {
    var that = this
    this.setData({
      isAddPopup: !this.data.isAddPopup,
    })
    if (this.data.isAddPopup === true) {
      this.setData({
        isAddShowPopup: true
      })
    } else {
      setTimeout(function() {
        that.setData({
          isAddShowPopup: false
        })
      }, 500);
    }
  },

  // 卡片弹出层显示
  showMoreInfo: function(e) {
    var that = this
    // 获取选中卡片的ID和标签
    var _id = e.currentTarget.id.split('-')[0];
    var _tag = e.currentTarget.id.split('-')[1];
    var _openId = app.globalData.openid;
    that.data.cOrTData.forEach((item) => {
      if (item._id === _id) {
        that.setData({
          currentCardTitle: item.title
        })
      }
    })
    that.setData({
      createUserId: _id
    })
    // 进行数据库查询获取详细信息
    if (_tag === "collect") {
      this.queryCollectsByOpenIdAndId(_openId, _id);
    } else if (_tag === "vote") {
      // 查询投票集合
      this.queryTicketsByOpenIdAndId(_openId, _id);
    }
    // 从数据库查询的数据会得到该数据标签
    var _listId = _tag;
    // 获取该卡片类型
    var _lists = this.data.lists;
    _lists.forEach(item => {
      if (item.id === _listId) {
        this.setData({
          currentList: item
        })
      }
    })
    that.setData({
      isPopup: true
    })
    that.setData({
      isShowCardInfo: true
    })
    if (this.data.isShowUserInfo == true) {
      this.toggleUserPopup();
    }
  },
  // 修改卡片状态
  toggleIsWrite: function() {
    var _currentInfo = this.data.currentInfo;
    _currentInfo['isWrite'] = !_currentInfo['isWrite']
    this.setData({
      currentInfo: _currentInfo
    })
  },
  // 表单状态变化
  toggleForm: function(formData) {
    var _currentInfo = this.data.currentInfo;
    _currentInfo['value'] = formData;
    this.setData({
      currentInfo: _currentInfo
    })
    this.toggleIsWrite();
  },
  // 卡片收集表单提交
  collectFormSubmit: function(e) {
    var self = this;
    var formData = e.detail.value; // 格式为：type: value
    // 如果未完全填写则提示错误
    for (var key in formData) {
      if (formData[key] === '') {
        // 提示输入错误
        wx.showModal({
          title: '提示',
          content: '请填写全部信息',
        })
        return;
      }
    }
    var _currentTime = dateFormatUtil.formatTime(new Date());
    // 获取当前卡片用户是否以及填写过
    var _from = self.data.currentInfo['from'];
    // 用户尚未填写，添加到collectResult
    if (_from === 'collects') {
      // 保存到collectResult数据集合中
      this.addCollectResult(self.data.createUserId, _currentTime, formData, self.data.userInfo);
    } else if (_from === 'collectResult') { // 用户修改
      this.updateCollectResultById(self.data.currentInfo['id'], formData, _currentTime);
    }
  },

  // 卡片投票表单提交
  voteFormSubmit: function(e) {
    var self = this;
    // 获取当前投票内容
    var voteChooseValue = this.data.currentInfo.value;
    // 获取当前投票类别(true为多选，false为单选)
    var isMultiSelect = this.data.currentInfo.isMultiSelect;
    // 获取当前投票是否匿名
    var isAnonymity = this.data.currentInfo.isAnonymity;
    // 获取当前投票信息id
    var createUserId = this.data.currentInfo.id;
    // 获取当前用户信息
    var userInfo = this.data.userInfo;
    // 获取投票结果
    var resultValue = e.detail.value.group;
    // 投票时间
    var createTime = dateFormatUtil.formatTime(new Date());
    // 将投票结果更新到pollvote集合中
    this.updatePollvoteByUpperIdAndGroup(createUserId, resultValue, isMultiSelect);
    // 将投票人员信息添加到ticketsResult集合中
    this.addTicketResultByUpperId(createUserId, createTime, userInfo, resultValue, isMultiSelect, isAnonymity);
    // 获取投票结果数据
    setTimeout(function () {
      self.queryPollvoteByUpperId(createUserId);
      self.queryTicketResultByUpperId(createUserId);
    }, 2000)
  },
  // 卡片弹出层隐藏
  hideMoreInfo: function(e) {
    var that = this
    that.setData({
      isPopup: false
    })
    that.setData({
      currentInfo: [],
      ticketResult: {},
      ticketAvators: []
    })
    setTimeout(function() {
      that.setData({
        isShowCardInfo: false
      })
    }, 200)
  },

  // 添加内容弹出层控制
  toggleCreatePopup: function() {
    var that = this;
    this.setData({
      isCreatePopup: !this.data.isCreatePopup,
      isForm: true
    })
    if (this.data.isCreatePopup === true) {
      setTimeout(function() {
        that.setData({
          isShowCreateInfo: true
        })
      })
    } else {
      setTimeout(function() {
        that.setData({
          isShowCreateInfo: false
        })

        // 在卡片隐藏的时候去更新数据
        that.queryCollectsAndTickets(app.globalData.openid);
        that.queryNotices(app.globalData.openid);

        // 清除缓存
        wx.removeStorage({
          key: 'formData',
          success: function(res) {
            console.log('成功清除缓存');
          },
        })

      }, 500)
    }
  },

  // 功能跳转
  goToOtherPage: function(e) {
    var _url = e.currentTarget.id;
    var that = this;
    wx.redirectTo({
      url: "../" + _url + "/" + _url,
      fail: res => {
        wx.showModal({
          title: "非常抱歉",
          content: "程序员被祭天了,该功能还未上线",
        })
      },
      complete: (res) => {
        that.toggleUserPopup();
      }
    })
  },

  // 点击快速添加按钮
  quicklyAddClick: function(e) {
    // 获取添加类型
    var _id = e.currentTarget.id;
    var _lists = this.data.lists;
    _lists.forEach(item => {
      if (item.id === _id) {
        this.setData({
          currentList: item
        })
      }
    })
    // 隐藏按钮弹出层
    this.toggleAddPopup();
    // 显示内容添加弹出层
    this.toggleCreatePopup();
  },

  // 添加内容表单提交
  formSubmit: function(e) {
    // 获取当前输入表单标签
    var tag = this.data.currentList.id;
    console.log(e)
    // 获取表单内容
    var formValue = e.detail.value;
    // 获取标题和内容
    var title = formValue.title;
    var content = formValue.content;
    if (title === '' || content === '') {
      wx.showModal({
        title: '提示',
        content: '请输入标题或内容',
      })
      return;
    }
    // 获取创建时间
    var createTime = dateFormatUtil.formatTime(new Date());
    // 封装数据
    // 获取数据类型
    var _current = this.data.currentList;
    var _data = {
      'title': title,
      'content': content,
      'type': _current,
      'user': this.data.userInfo.nickName,
      'createTime': createTime
    };
    this.setData({
      formContent: _data
    })
    // 输入结束，显示结果
    this.setData({
      isForm: false
    })
    if (tag === 'vote') {
      var isAnonymity = formValue.anonymity;
      var isMultiSelect = formValue.multiSelect;
      _data['isAnonymity'] = isAnonymity;
      _data['isMultiSelect'] = isMultiSelect;
    }
    // // 将数据存到缓存中
    // wx.setStorage({
    //   key: 'formData',
    //   data: _data,
    // })
    // 存储数据
    this.addFormValue(_data);
  },

  /**
   * 添加表单数据到数据库
   */
  addFormValue: function(_data) {
    var tag = _data.type.id;
    if (tag === "collect") {
      this.addCollects(_data);
    } else if (tag === "noticeWall") {
      this.addNotices(_data);
    } else if (tag === "vote") {
      this.addTickets(_data);
    }
  },

  /**
   * 删除快速添加界面下刚创建的内容
   */
  deleteCreateValue: function() {
    // 删除当前创建的数据
    this.deleteValue(this.data.currentList.id);
    this.toggleCreatePopup();
  },

  deleteValue: function(tag) {
    if (tag === 'vote') {
      this.deleteTicketsById(this.data.createSuccessId);
      this.deletePollvoteByUpperId(this.data.createSuccessId);
    } else if (tag === 'collect') {
      this.deleteCollectsById(this.data.createSuccessId);
    } else if (tag === 'noticeWall') {
      this.deleteNoticesById(this.data.createSuccessId);
    }
  },

  // 通告轮播触发
  cardSwiper: function(e) {
    // console.log('cardSwiper', e);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    let self = this;
    var query = app.globalData.query;
    app.getShareTiket(function (globalData) {
      console.log('[noticeWall.js] [onReady] globalData is ' + JSON.stringify(globalData))
    })
    if (query['id'] === undefined || query['id'] === '') {
      // do notion
    } else {
      this.showMassageFromGroupChat(query);
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // wx.getShareInfo({
    //   shareTicket: app.shareTicket,
    //   success: (res) => {
    //     console.log(res)
    //   }
    // })
    wx.showShareMenu({
      'withShareTicket': true
    })
    // 查询当前用户的所有通知
    var openid = app.globalData.openid;
    this.queryNotices(openid);
    // 查询当前用户的所有收集或投票
    this.queryCollectsAndTickets(openid);

    const db = wx.cloud.database();
    db.collection('collectResult').where({
      _openid: 'o87_G5Yx7z02i89SEBKkl26sHslE',
      upperId: 'ee3099285cda877e11e05f7239d7ddf8'
    }).count({
      success(res) {
        console.log(res)
      }
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(res) {
    console.log(res)
    var self = this;
    // // 获取要存储的缓存数据
    // wx.getStorage({
    //   key: 'formData',
    //   success: function(res) {
    //     // 进行存储
    //     self.addFormValue(res.data);
    //   },
    // })
    self.toggleCreatePopup();
    return {
      title: '您收到了来自' + self.data.formContent.user + '的' + self.data.formContent.type.tag,
      path: '/pages/noticeWall/noticeWall?id=' + self.data.createSuccessId + '&tag=' + self.data.formContent.type.id,
    }
  },

  /**
   * 从群聊中分享的卡片消息进到小程序
   * 
   * @param query Object
   * 
   * query应该由以下几个字段组成：tag;id;
   */
  showMassageFromGroupChat: function (query) {
    var self = this;
    // 获取id和tag
    var _tag = query['tag'];
    var _id = query['id'];
    // 打开详细信息弹出层
    var _openId = app.globalData.openid;
    self.setData({
      createUserId: _id
    })
    // 进行数据库查询获取详细信息
    if (_tag === "collect") {
      this.queryCollectsByOpenIdAndId(_openId, _id);
      this.getMessageTitleByIdAndCollection(_id, 'collects');
    } else if (_tag === "vote") {
      // 查询投票集合
      this.queryTicketsByOpenIdAndId(_openId, _id);
      this.getMessageTitleByIdAndCollection(_id, 'tickets');
    }
    // 从数据库查询的数据会得到该数据标签
    var _listId = _tag;
    // 获取该卡片类型
    var _lists = this.data.lists;
    _lists.forEach(item => {
      if (item.id === _listId) {
        this.setData({
          currentList: item
        })
      }
    })
    self.setData({
      isPopup: true
    })
    self.setData({
      isShowCardInfo: true
    })
    if (this.data.isShowUserInfo == true) {
      this.toggleUserPopup();
    }
  },

  //=========================数据库语句===============================


  /*------------collects集合的增删改查 start------------------------*/

  /**
   * 添加数据到collects集合
   */
  addCollects: function(_data) {
    const db = wx.cloud.database();
    var self = this;
    db.collection('collects').add({
      data: {
        _data,
        serverTime: db.serverDate()
      },
      success(res) {
        // 返回创建后的_id
        self.setData({
          createSuccessId: res._id
        })
      },
      fail: console.error
    })
  },

  /**
   * 查询当前用户指定收集卡片的信息
   */
  queryCollectsByOpenIdAndId: function(openId, id) {
    const db = wx.cloud.database();
    var that = this;
    // 查询用户有无填写
    wx.cloud.callFunction({
      name: "isUserWriteInfo",
      data: {
        openId: openId,
        upperId: id,
        databaseName: 'collectResult'
      },
      success: (res) => {
        if (res.result.total > 0) {
          // 有结果，去collectResult集合查询
          that.queryCollectResultByOpenIdAndId(openId, id);
        } else {
          // 没有结果，去collects集合查询
          that.queryCollectsById(id);
        }
      }
    })
  },

  /**
   * 通过id去查询collects集合
   */
  queryCollectsById: function(id) {
    const db = wx.cloud.database();
    var that = this;
    db.collection('collects').where({
      _id: id
    }).get({
      success(res) {
        res.data.forEach((item) => {
          var collectionContentList = item._data.content.split("；");
          var value = {}

          for (var i = 0; i < collectionContentList.length; i++) {
            if (collectionContentList[i] === '') {} else {
              value[collectionContentList[i]] = '';
            }
          }

          var currentFormValue = {
            'id': res.data[0]._id,
            'value': value,
            'isWrite': false,
            'from': 'collects'
          }
          that.setData({
            currentInfo: currentFormValue
          })
        })
      }
    })
  },

  /**
   * 通过_id删除记录
   */
  deleteCollectsById: function(id) {
    const db = wx.cloud.database();
    db.collection('collects').doc(id).remove({

    })
  },

  /*------------collects集合的增删改查 end--------------------------*/


  /*------------collectResult集合的增删改查 start-------------------*/

  /**
   * 将数据添加到collectResult集合中
   */
  addCollectResult: function(upperId, createTime, formData, userInfo) {
    const db = wx.cloud.database();
    var self = this;
    db.collection('collectResult').add({
      data: {
        upperId: upperId,
        createTime: createTime,
        value: formData,
        userInfo: userInfo,
        serverTime: db.serverDate()
      },
      success(res) {
        // 保存成功
        self.toggleForm(formData);
      },
      fail(err) {
        console.log(err)
      }
    })
  },

  /**
   * 通过opedId和Id查询collectResult集合
   */
  queryCollectResultByOpenIdAndId: function(openId, id) {
    const db = wx.cloud.database();
    var that = this;
    db.collection('collectResult').where({
      _openid: openId,
      upperId: id
    }).get({
      success(res) {
        var result = res.data[0];
        var currentFormValue = {
          'id': result._id,
          'value': result.value,
          'isWrite': true,
          'from': 'collectResult'
        }
        that.setData({
          currentInfo: currentFormValue
        })
      }
    })
  },

  /**
   * 通过id更新collectResult集合
   */
  updateCollectResultById: function(id, formData, updateTime) {
    var self = this;
    const db = wx.cloud.database();
    db.collection('collectResult').doc(id).update({
      data: {
        value: formData,
        updateTime: updateTime
      },
      success(res) {
        self.toggleForm(formData);
      }
    })
  },

  /*------------collectResult集合的增删改查 end---------------------*/


  /*------------notices集合的增删改查 start-------------------------*/

  /**
   * 添加数据到notices集合
   */
  addNotices: function(_data) {
    const db = wx.cloud.database();
    var self = this;
    // 通告
    db.collection('notices').add({
      data: {
        _data,
        serverTime: db.serverDate()
      },
      success(res) {
        // 返回创建后的_id
        self.setData({
          createSuccessId: res._id
        })
      },
      fail: console.error
    })
  },

  /**
   * 查询该用户所有的通告
   */
  queryNotices: function(openId) {
    var self = this;
    var notices = [];
    const db = wx.cloud.database()
    db.collection('notices').where({
      _openid: openId
    }).orderBy('_data.createTime', 'desc').get({
      success(res) {
        res.data.forEach((item) => {
          notices.push(item._data)
        })

        self.setData({
          noticeInfos: notices
          // showSkeleton: true
        })
      },
      fail(err) {
        console.log(err)
      }
    })
  },

  /**
   * 通过_id删除
   */
  deleteNoticesById: function(id) {
    const db = wx.cloud.database();
    db.collection('notices').doc(id).remove({

    })
  },


  /*------------notices集合的增删改查 end---------------------------*/


  /*------------tickets集合的增删改查 start-------------------------*/

  /**
   * 添加数据到tickets集合
   */
  addTickets: function(_data) {
    var self = this;
    const db = wx.cloud.database();
    // 投票
    db.collection('tickets').add({
      data: {
        _data,
        serverTime: db.serverDate()
      },
      success(res) {
        var _id = res._id;
        var createTime = _data.createTime;
        var groups = {}
        var contents = _data.content.split('；');
        for (var i = 0; i < contents.length; i++) {
          if (contents[i] === '') {} else {
            // 初始化投票结果
            groups[contents[i]] = 0;
          }
        }
        // 创建结果记录
        self.addPollvate(_id, createTime, groups, 0);
        self.setData({
          createSuccessId: _id
        })
      },
      fail: console.error
    })
  },

  /**
   * 查询当前用户所选中的投票卡片信息
   */
  queryTicketsByOpenIdAndId: function(openId, upperId) {
    const db = wx.cloud.database();
    var self = this;
    // 查询用户是否已经投票
    wx.cloud.callFunction({
      name: "isUserWriteInfo",
      data: {
        openId: openId,
        upperId: upperId,
        databaseName: 'ticketResult'
      },
      success: (res) => {
        if (res.result.total > 0) {
          // 有结果，去ticketResult集合以及pollvote集合查询
          self.queryPollvoteByUpperId(upperId);
          self.queryTicketResultByUpperId(upperId);
        } else {
          // 没有结果，去tickets集合查询
          self.queryTicketsById(upperId);
        }
      }
    })
  },

  /**
   * 通过id查询tickets集合
   */
  queryTicketsById: function(id) {
    const db = wx.cloud.database();
    var self = this;
    db.collection('tickets').where({
      _id: id
    }).get({
      success(res) {
        res.data.forEach((item) => {
          var ticketContentList = item._data.content.split("；");
          var value = {}

          for (var i = 0; i < ticketContentList.length; i++) {
            if (ticketContentList[i] === '') {} else {
              value[i] = ticketContentList[i]
            }
          }

          var currentFormValue = {
            'id': item._id,
            'value': value,
            'isWrite': false,
            'isAnonymity': item._data.isAnonymity,
            'isMultiSelect': item._data.isMultiSelect
          }
          self.setData({
            currentInfo: currentFormValue
          })
        })
      },
      fail(err) {
        console.log(err)
      }
    })
  },

  deleteTicketsById: function(id) {
    const db = wx.cloud.database();
    db.collection('tickets').doc(id).remove({
    })
  },

  /*------------tickets集合的增删改查 end---------------------------*/


  /*------------ticketResult集合的增删改查 start--------------------*/

  /**
   * 添加记录到ticketResult集合
   */
  addTicketResultByUpperId: function(upperId, createTime, userInfo, result, isMulSelect, isAnonymity) {
    const db = wx.cloud.database();
    if(isAnonymity) {
      db.collection('ticketResult').add({
        data: {
          upperId,
          upperId,
          createTime: createTime,
          userInfo: {},
          isMulSelect: isMulSelect,
          isAnonymity: isAnonymity,
          voteResult: result,
          serverTime: db.serverDate()
        }
      })
    } else {
      db.collection('ticketResult').add({
        data: {
          upperId,
          upperId,
          createTime: createTime,
          userInfo: userInfo,
          isMulSelect: isMulSelect,
          isAnonymity: isAnonymity,
          voteResult: result,
          serverTime: db.serverDate()
        }
      })
    }
    
  },

  /**
   * 查询该upperId(投票id)的所有投票记录
   */
  queryTicketResultByUpperId: function(upperId) {
    const db = wx.cloud.database();
    var self = this;
    db.collection('ticketResult').where({
      upperId: upperId
    }).get({
      success(res) {
        var array = res.data;
        var avators = [];
        if(!array[0].isAnonymity) {// 不是匿名投票
          // 获取用户头像组
          for (var i = 0; i < array.length; i++) {
            avators[i] = array[i].userInfo.avatarUrl;
          }
          self.setData({
            ticketAvators: avators
          })
        }
      }
    })
  },


  /*------------ticketResult集合的增删改查 end----------------------*/


  /*------------pollvote集合的增删改查 start------------------------*/

  /**
   * 创建一条投票结果记录
   */
  addPollvate: function(upperId, createTime, groups, total) {
    const db = wx.cloud.database();
    db.collection('pollvote').add({
      data: {
        upperId: upperId,
        createTime: createTime,
        groups: groups,
        total: total,
        serverTime: db.serverDate()
      },
      success(res) {

      },
      fail(err) {

      }
    })
  },

  /**
   * 更新投票记录
   * 
   * @param upperId 查询条件
   * @param groups 投票的结果
   * @param isMulSelect 多选还是单选 (true是多选，false是单选)
   */
  updatePollvoteByUpperIdAndGroup: function(upperId, groups, isMulSelect) {
    var self = this;
    const db = wx.cloud.database();
    // 获取当前投票结果
    db.collection('pollvote').where({
      upperId: upperId
    }).get({
      success(res) {
        // 获取记录
        var groupList = res.data[0].groups;
        var total = res.data[0].total;
        if (isMulSelect) {
          // 多选
          groups.forEach((item) => {
            groupList[item] += 1;
            total += 1;
          })
        } else {
          // 单选
          groupList[groups] += 1;
          total += 1;
        }
        // 更新记录
        db.collection('pollvote').doc(res.data[0]._id).update({
          data: {
            groups: groupList,
            total: total
          },
          success(res) {
            // 修改表单状态
            self.toggleForm();
          },
          fail(err) {}
        })
      }
    })
  },

  /**
   * 查询upperId投票结果
   */
  queryPollvoteByUpperId: function(upperId) {
    const db = wx.cloud.database();
    var self = this;
    db.collection('pollvote').where({
      upperId: upperId
    }).get({
      success(res) {
        // 获取投票结果
        var result = res.data[0].groups;
        // 获取结果总数
        var total = res.data[0].total;
        self.setData({
          ticketTotal: total,
          ticketResult: result,
        })
      }
    })
  },

  /**
   * 通过upperId来删除记录
   */
  deletePollvoteByUpperId: function(upperId) {
    const db = wx.cloud.database();
    // 在删除条件不是_id时，需要先查到该条信息的_id再执行删除操作
    db.collection('pollvote').where({
      upperId: upperId
    }).get({
      success(res) {
        db.collection('pollvote').doc(res.data[0]._id).remove({

        })
      }
    })
  },

  /*------------pollvote集合的增删改查 end--------------------------*/


  /**
   * 查询当前用户所有的收集和投票
   */
  queryCollectsAndTickets: function(openId) {
    const db = wx.cloud.database();
    var _cOrTData = [];
    var self = this;
    db.collection('collects').where({
      _openid: openId
    }).orderBy('_data.createTime', 'desc').get({
      success(res) {
        res.data.forEach((item) => {
          // 添加唯一标识符到_data中
          item._data['_id'] = item._id
          _cOrTData.push(item._data);
        })

        db.collection('tickets').where({
          _openid: openId
        }).orderBy('_data.createTime', 'desc').get({
          success(res) {
            res.data.forEach((item) => {
              item._data['_id'] = item._id;
              _cOrTData.push(item._data);
            })

            // 最后结果进行排序
            _cOrTData.sort(function(a, b) {
              return a.createTime < b.createTime ? 1 : -1;
            })

            self.setData({
              cOrTData: _cOrTData
            })
          },
          fail(err) {
            console.log(err)
          }
        })
      },
      fail(err) {
        console.log(err)
      }
    })
  },

  /**
   * 通过id获取指定集合记录中的title
   */
  getMessageTitleByIdAndCollection: function (_id, collection) {
    var self = this;
    const db = wx.cloud.database();
    db.collection(collection).doc(_id).get({
      success(res) {
        self.setData({
          currentCardTitle: res.data._data.title
        })
      }
    })
  }

})